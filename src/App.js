import React, {Component} from 'react';
import {Provider} from 'rebass';
import HeaderContainer from "./Containers/HeaderContainer";
import Footer from "./components/footer/Footer";
import Main from "./Main";
import {BrowserRouter as Router} from "react-router-dom";

class App extends Component {
  render() {
    return (
      <Router>
        <Provider>
          <HeaderContainer/>
          <Main/>
          <Footer/>
        </Provider>
      </Router>
    );
  }
}

export default App;
