import React, {Component} from 'react';
import Header from '../components/header/Header'
import Navigation from '../components/Navigation';
import {Box} from 'rebass';

class HeaderContainer extends Component {
  render() {
    return (
      <Box>
        <Header/>
        <Navigation/>
      </Box>
    );
  }
}

export default HeaderContainer;