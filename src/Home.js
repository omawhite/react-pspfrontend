import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Box} from "rebass";

class Home extends Component {
  render() {
    return(
      <Box mx={'auto'}>
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </div>
      </Box>
    );
  }
}

export default Home