import React from 'react';
import './header.css';
import Logo from '../logo/Logo';
import {Button, Flex} from 'rebass';

function Header() {
    return(
        <Flex bg='#4490F7' px={'5px'} py={'10px'}>
          <Logo/>
          <Button ml={'auto'} mr={'50px'} children={'Login'} />
        </Flex>
    )
}

export default Header;