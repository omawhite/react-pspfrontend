import React from 'react';
import {Flex} from 'rebass';
import StyledNav from "./styledNav/StyledNav";

function Navigation() {
  return (
    <Flex bg={'grey'}>
      <StyledNav
        to={"/"}
        style={{
          color: '#fff',
          fontWeight: 'bold',
        }}>
        Home
      </StyledNav>
      <StyledNav
        to={"/"}
        style={{
          color: '#fff',
          fontWeight: 'bold',
        }}>
        Topic 1
      </StyledNav>
      <StyledNav
        to={"/"}
        style={{
          color: '#fff',
          fontWeight: 'bold',
        }}>
        Topic 2
      </StyledNav>
    </Flex>
  )
}

export default Navigation