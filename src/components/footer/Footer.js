import React from 'react';
import {Box, Flex, Text} from "rebass";

function Footer() {
    return(
      <Flex bg={'#4490F7'}>
      <Box mx={'auto'} py={'10px'} px={'10px'}>
        <Text
          textAlign={'center'}
          color={'#fff'}
          children={'People Sourced Policy A Project of the Center for Policy Design'}/>
      </Box>
      </Flex>
    );
}

export default Footer;