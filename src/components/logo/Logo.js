import React from 'react';
import './logo.css';
import logo from './logo.png';

function Logo() {
    return(
        <div class="logo">
          <a href="http://peoplesourcedpolicy.org">
            <img src={logo} alt="Psp Logo" width="300"/>
          </a>
        </div>
    )
}

export default Logo;
