import React from 'react';
import {NavLink} from 'react-router-dom';
import styled from 'styled-components';


const StyledNavLink = styled(NavLink)`
    text-decoration: none;
    margin: 5px;

    &:focus, &:hover, &:visited, &:link, &:active {
        text-decoration: none;
    }
`;

export default (props) => <StyledNavLink {...props} />;