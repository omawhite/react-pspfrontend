import React, {Component} from 'react';
import {Route} from "react-router-dom";
import Home from "./Home";


class Main extends Component {
  render() {
    return (
      <section>
        <Route exact path={'/'} component={Home} />
      </section>
    );
  }
}

export default Main